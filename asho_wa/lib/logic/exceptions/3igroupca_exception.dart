class GroupCaException implements Exception {
  final String _message;
  GroupCaException([this._message = ""]);

  @override
  String toString() {
    if (_message.isEmpty) return "Exception";
    return "Exception: $_message";
  }

  //get message
  String getMessage() {
    return _message;
  } //end getMessage
}
