// ignore_for_file: public_member_api_docs, sort_constructors_first

//state for registering step

class InscriptionState {
  bool hasImage;
  bool hasClicked = false;
  bool hasErrorVerify;
  bool hasError = false;
  bool passVisible = false;
  bool confirmPassVisible = false;
  String phone;
  String firstName;
  String email;
  String lastName;
  String password;
  int maxNumber;
  int indexCode;
  String dialCode;


  String messageError = "";
  String currentCode = "";

  InscriptionState(
      {required this.hasImage,
      required this.hasClicked,
      required this.hasError,
      required this.hasErrorVerify,
      required this.passVisible,
      required this.confirmPassVisible,
      required this.phone,
      required this.firstName,
      required this.lastName,
      required this.email,
      required this.password,
      required this.dialCode,
      required this.maxNumber,
      required this.indexCode,
      required this.messageError,
      required this.currentCode});
}
