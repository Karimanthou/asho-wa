import 'package:flutter_bloc/flutter_bloc.dart';

import 'inscription_state.dart';

class InscriptionCubit extends Cubit<InscriptionState> {

  InscriptionCubit()
      : super(InscriptionState(
          hasImage: false,
          hasClicked: false,
          passVisible: false,
          confirmPassVisible: false,
          dialCode: '+229',
          //countryCode: 'BJ',
          maxNumber: 8,
          indexCode: 0,
          hasError: false,
          currentCode: '',
          firstName: '',
          hasErrorVerify: false,
          lastName: '',
          messageError: '',
          password: '',
          phone: '',
      
          email: '',
         
        )) {
    // roles();
  }

//get Country Code And maxNumber
  getCountryCodeAndMaxNumber(
      {required String dialCode, required int maxLength}) {
    state.dialCode = dialCode;
    state.maxNumber = maxLength;
    print(state.dialCode);
    print(state.maxNumber);
    emit(InscriptionState(
        passVisible: state.passVisible,
        confirmPassVisible: state.confirmPassVisible,
        hasImage: state.hasImage,
        hasClicked: state.hasClicked,
        dialCode: dialCode,
     
        messageError: state.messageError,
        hasError: state.hasError,
        //// countryCode: countryCode,
        currentCode: state.currentCode,
        indexCode: state.indexCode,
        maxNumber: maxLength,
        email: state.email,
        lastName: state.lastName,
        password: state.password,
        firstName: state.firstName,
        hasErrorVerify: false,
        phone: state.phone,
       ));
  } //end getCountryCodeAndMaxNumber

//check phone number
  String? validatePhone({required String? phone}) {
    if (phone!.isEmpty) {
      return "Veuillez saisir votre numéro de téléphone";
    } else if (phone.length != state.maxNumber) {
      return "Veuillez saisir un numéro valide";
    }
    //var num = state.dialCode + phone;
    state.phone = state.dialCode + phone;
    print("state.phone");
    print(state.phone);
    emit(state);

    return null;
  } //end validatePhone

//check firstname
  String? validateFirstName({required String? firstName}) {
    if (firstName!.isEmpty) {
      return 'Veuillez saisir votre nom';
    }
    state.firstName = firstName;
    print(state.firstName);
    emit(state);
    return null;
  }


//check email
  String? validateEmail({required String? mail}) {
    Pattern pattern = r'^[a-zA-Z.]+@[a-zA-Z.]';
    RegExp regex = RegExp(pattern as String);
    if (mail!.isEmpty) {
      return 'Veuillez saisir votre adresse email';
    } else if (!regex.hasMatch(mail)) {
      return 'Veuillez saisir une adresse email valide';
    }
    state.email = mail;
    emit(state);
    return null;
  }

//check lastname
  String? validateLastName({required String? lastName}) {
    if (lastName!.isEmpty) {
      return 'Veuillez saisir votre prénom';
    }
    state.lastName = lastName;
    emit(state);

    return null;
  } //end validateLastName

//check  password
  String? validatePassword({required String? password}) {
    if (password!.isEmpty) {
      return 'Veuillez saisir votre mot de passe';
    } else if (password.length < 5) {
      return 'Veuillez saisir 5 caractères au moins';
    }
    state.password = password;
    emit(state);
    return null;
  } //end validatePassword

//check confirmation password
  String? validateConfirmationPassword(
      {required String? confirmPass, required String? password}) {
    if (confirmPass!.isEmpty) {
      return 'Veuillez confirmer votre mot de passe';
    }
    if (confirmPass.length < 5) {
      return 'Veuillez saisir 5 caractères au moins';
    } else if (confirmPass != password) {
      return "Mot de passe et confirmation non identiques";
    }
    return null;
  } //end validateConfirmationPassword

//check visibility
  void passVisibility(bool visible) {
    visible = !visible;

    emit(InscriptionState(
        phone: state.phone,
        firstName: state.firstName,
        lastName: state.lastName,
        password: state.password,
        hasImage: state.hasImage,
        hasClicked: state.hasClicked,
        dialCode: state.dialCode,
        maxNumber: state.maxNumber,
        passVisible: visible,
        email: state.email,
        confirmPassVisible: state.confirmPassVisible,
        messageError: state.messageError,
        hasError: state.hasError,
        currentCode: state.currentCode,
        hasErrorVerify: false,
        indexCode: 0,
       ));
  } //end passVisibility

//check visibility
  void confirmPassvisibility(bool visible) {
    visible = !visible;

    emit(InscriptionState(
        phone: state.phone,
        firstName: state.firstName,
        lastName: state.lastName,
        password: state.password,
        hasImage: state.hasImage,
        hasClicked: state.hasClicked,
        dialCode: state.dialCode,
        maxNumber: state.maxNumber,
        passVisible: state.passVisible,
        confirmPassVisible: visible,
        messageError: state.messageError,
        hasError: state.hasError,
        email: state.email,
        currentCode: state.currentCode,
        hasErrorVerify: state.hasErrorVerify,
        indexCode: 0));
  } //end confirmPassvisibility
}
