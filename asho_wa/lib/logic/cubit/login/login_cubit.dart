import 'package:ASHOWA/logic/utils/utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {

  LoginCubit()
      : super(LoginState(
          hasError: false,
          messageError: '',
          nom: '',
          prenom: '',
          reference: "",
          telephone: '',
          email: '',
        ));

  getDialCode(String dialCode) {
    Utils.displayData("dialcode", dialCode);

    emit(LoginState(
        messageError: state.messageError,
        nom: state.nom,
        prenom: state.prenom,
        reference: state.reference,
        telephone: state.telephone,
        hasError: false,
        email: state.email));
  }

  getProfil() async {
  emit(LoginState(
        hasError: state.hasError,
        nom: state.nom,
        prenom: state.prenom,
        email: state.email,
        telephone: state.telephone,
        reference: state.reference,
        messageError: state.messageError));
  }
}
