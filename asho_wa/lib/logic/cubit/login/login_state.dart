// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'login_cubit.dart';

class LoginState {
  String messageError;
  String nom;
  String prenom;
  String telephone;
  String email;
  String reference;
  bool hasError = false;
  LoginState(
      {required this.nom,
      required this.prenom,
      required this.telephone,
      required this.reference,
      required this.email,
      required this.hasError,
      required this.messageError});
}
