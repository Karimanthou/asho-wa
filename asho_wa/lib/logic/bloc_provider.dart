import 'package:ASHOWA/logic/cubit/home/home_cubit.dart';
import 'package:ASHOWA/logic/cubit/inscription/inscription_cubit.dart';
import 'package:ASHOWA/logic/cubit/login/login_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


List<BlocProvider> blocProviders = [
  BlocProvider<HomeCubit>(
    create: (contextHome) => HomeCubit(),
  ),
   BlocProvider<InscriptionCubit>(
    create: (inscriptionCubitContext) => InscriptionCubit(),
  ),
  BlocProvider<LoginCubit>(
    create: (loginCubitContext) => LoginCubit(),
  ),
];
