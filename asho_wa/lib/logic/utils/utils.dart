import 'package:flutter/material.dart';


class Utils {

  //does a print
  static void displayData(dynamic label, dynamic value) {
    // ignore: avoid_print
    print('$label: $value');
  } //end displayData

  /// dosomthing
  static convertToDatetime(String date) {
    DateTime date1;
    if (date == "" || date.isEmpty) {
      date1 = DateTime.now().toLocal();
      return date1;
    } else {
      date1 = DateTime.parse(date).toLocal();
      return date1;
    }
  }

  //
  static fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  } //end

}
