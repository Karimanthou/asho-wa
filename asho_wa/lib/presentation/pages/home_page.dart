import 'package:ASHOWA/logic/cubit/home/home_cubit.dart';
import 'package:ASHOWA/presentation/pages/acceuil.dart';
import 'package:ASHOWA/presentation/pages/favori.dart';
import 'package:ASHOWA/presentation/pages/panier.dart';
import 'package:ASHOWA/presentation/pages/parametre.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, this.tabIndex = 0}) : super(key: key);
  final int tabIndex;

  @override
  State<MyHomePage> createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  String title = '';
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _selectedIndex =
        BlocProvider.of<HomeCubit>(context, listen: true).state.selectedIndex;
    Widget widget = Container();
    switch (_selectedIndex) {
      case 0:
        widget =  AcceuilPage();
        break;
      case 1:
        widget =const PanierPage();
        break;
      case 2:
        widget = const FavoriPage();
        break;
      case 3:
        widget = ParametrePage();
        break;
    
    }
    return Builder(builder: (context) {
      return Scaffold(
        extendBodyBehindAppBar: true,
        // appBar: MyAppBar(title: Text(widget.title),),
        bottomNavigationBar: _bottomTab(),
        body: widget,
      );
    });
  }

  Widget _bottomTab() {
    return Container(
      height: 80,
      width: double.infinity,
      child: BottomNavigationBar(
         backgroundColor: Colors.white,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedFontSize: 24,
          unselectedFontSize: 24,
          //selectedItemColor: const Color(0xFF007BFF),
          unselectedItemColor: Colors.black54,
          selectedLabelStyle: const TextStyle(
              fontSize: 12,
              fontFamily: 'SFProDisplay-Semibold',
              color: Color(0xff007BFF)),
          unselectedLabelStyle: TextStyle(
              fontSize: 12,
              fontFamily: 'SFProDisplay-Semibold',
              color: Colors.black.withOpacity(0.6)),
              type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Boutique",
          
          ),
          
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart_outlined), label: "Panier"),
              BottomNavigationBarItem(
                icon: Icon(Icons.favorite_border), label: 'Favoris'),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), label: "Paramètre"),
        ],
       currentIndex: BlocProvider.of<HomeCubit>(context, listen: false)
              .state
              .selectedIndex,
          onTap: BlocProvider.of<HomeCubit>(context).onItemTapped
      ),
    ); // This tra  );
  }
}
