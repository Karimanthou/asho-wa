import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:flutter/material.dart';

class MesCommandesPage extends StatelessWidget {
  const MesCommandesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar("Mes Commandes"),
      body: ListView.builder(
       padding: const EdgeInsets.all(12),
        itemBuilder: (BuildContext context, int index) {
        return commandeBody(context);
      },
      itemCount: 2,),
    );
  }
}