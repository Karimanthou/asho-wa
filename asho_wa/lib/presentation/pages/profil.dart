import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:flutter/material.dart';

class Profil extends StatelessWidget {
  Profil({super.key});

  String nom = "";
  String prenom = "";
  String tel = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar("Profil"),
        body: ListView(
          padding: const EdgeInsets.all(20),
          children: [
           
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Nom"),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "KARIMOU",
                    style: TextStyle(color: Color(0xffFFA800)),
                  ),
                ],
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Prénom"),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Karimanthou",
                    style: TextStyle(color: Color(0xffFFA800)),
                  ),
                ],
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Téléphone"),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "+22996466513",
                    style: TextStyle(color: Color(0xffFFA800)),
                  ),
                ],
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text("Mot de passe"),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "********",
                              style: TextStyle(color: Color(0xffFFA800)),
                            ),
                            IconButton(
                                onPressed: () {
                                   Navigator.of(context).pushNamed(
                                      Routes.changementMdp,
                                    ); 
                                },
                                icon: const Icon(
                                  Icons.edit_outlined,
                                  color: Color(0xffFFA800),
                                  size: 20,
                                ))
                          ],
                        )
                ],
              ),
            ),
            const Divider(
              thickness: 1.5,
            ),
          ],
        ));
  }
}
