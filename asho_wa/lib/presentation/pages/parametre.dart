import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:flutter/material.dart';

class ParametrePage extends StatelessWidget {
  const ParametrePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar("Paramètres"),
      body: ListView(
        children: [
          parametreBody("Mon Profil", Icons.person_pin, () {
            Navigator.pushNamed(context, Routes.profil);
          }),
          parametreBody("Mes commandes", Icons.shopping_basket, () {
            Navigator.of(context).pushNamed(Routes.mesCommandes);
          }),
          parametreBody("Emplacement ASHOWA", Icons.map_outlined, () {}),
        ],
      ),
    );
  }
}
