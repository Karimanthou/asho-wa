import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:flutter/material.dart';

class FavoriPage extends StatelessWidget {
  const FavoriPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar("Mes Favoris"),
      body: ListView.builder(
        padding: EdgeInsets.all(12),
        itemBuilder: (BuildContext context, int index) {
         return bodyFavori(context, "images/bijou.jpg");
        },
      ),
    );
  }
}
