import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';

class ProduitPage extends StatelessWidget {
  ProduitPage({super.key});
  bool isCheck = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: body(context),
    );
  }

  appBar() {
    return AppBar(
      backgroundColor: Colors.white,
      foregroundColor: const Color(0xff27272e),
      elevation: 0.0,
      toolbarHeight: 70,
      bottom: PreferredSize(
          preferredSize: const Size.fromHeight(4.0),
          child: Container(
            color: const Color(0x1e425466),
            height: 2.0,
          )),
      //leading: Text(""),
      title: Container(
        //width: 90.w,
        child: TextFormField(
          //  controller: articleController,
          onChanged: ((value) {
            //   filterProduct(value);
          }),
          decoration: InputDecoration(
            labelText: 'Faites votre recherche',
            prefixIcon: const Icon(Icons.search_rounded),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          ),
          //controller: motdepasse,
        ),
      ),
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Recheche par Catégorie :",
            style: titreCategorie,
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.2,
          child: GridView.builder(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            itemCount: 5,
            itemBuilder: (context, index) => Container(
                //padding: EdgeInsets.all(10),
                //margin: EdgeInsets.only(right: 5),
                // height: 20,
                decoration: BoxDecoration(
                    color: isCheck
                        ? const Color(0xffBC6C25)
                        : const Color(0xff1A506D),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(8),
                    ),
                    shape: BoxShape.rectangle),
                child: Center(
                  child: TextButton(
                    onPressed: () {
                      isCheck = true;
                    },
                    child: const Text(
                      "Catégorie 1",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                )),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 2.0,
                mainAxisExtent: 40.0,
                crossAxisSpacing: 8.0,
                crossAxisCount: 3,
                mainAxisSpacing: 8.0),
          ),
        ),
        Expanded(
            child: GridView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
          itemCount: 5,
          itemBuilder: (context, index) => Container(
              
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                  shape: BoxShape.rectangle),
              child: Center(
                  child: tendanceWidget(context, "images/chaussure.jpg"))),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 2.0,
              mainAxisExtent: 300.0,
              crossAxisSpacing: 8.0,
              crossAxisCount: 2,
              mainAxisSpacing: 8.0),
        ))
      ],
    );
  }
}
