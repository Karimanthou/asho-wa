import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:flutter/material.dart';

enum Pet { livrer, entrepot }

class Reception extends StatelessWidget {
  const Reception({super.key});

  @override
  Widget build(BuildContext context) {
    Pet pet = Pet.entrepot;
    bool isMy = false;

    return Scaffold(
        appBar: appBar("Mode de Livraison"),
        body: ListView(
          padding: const EdgeInsets.all(15),
          children: [
            const Text(
              textAlign: TextAlign.center,
              'Comment préférez-vous recevoir votre commande ?',
              style: TextStyle(
                fontSize: 24,
                color: Colors.black,
                fontWeight: FontWeight.w500,
                //fontFamily: 'Times New Roman',
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Center(
              child: Text(
                //textAlign: TextAlign.center,
                'Choisissez une méthode de réception',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w300,
                  //fontFamily: 'Times New Roman',
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            ListTile(
              title: const Text(
                'Se faire livrer',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  //fontFamily: 'Times New Roman',
                ),
              ),
              //onTap: () => localise(),
              leading: Radio<Pet>(
                value: Pet.livrer,
                groupValue: pet,
                onChanged: (value) {
                  //setState(() {
                  isMy = false;
                  pet = value!;
                  print(pet.name);
                  //});
                },
              ),
            ),
            ListTile(
              title: const Text(
                "Venir à l’entrepôt",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  //fontFamily: 'Times New Roman',
                ),
              ),
              leading: Radio<Pet>(
                value: Pet.entrepot,
                groupValue: pet,
                onChanged: (value) {
                  // setState(() {
                  isMy = false;
                  pet = value!;

                  print(pet.name);
                  // });
                },
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            // Text(distanceInMeters.toStringAsFixed(0)),
            isMy
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        'Commune :',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          //fontFamily: 'Times New Roman',
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      // selectForm(listCom, nomCommune, "nomCommune"),
                    ],
                  )
                : const Text(""),
            const SizedBox(
              height: 10,
            ),

            Card(
              child: Container(
                padding: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Sous-total :",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.w300,
                            //fontFamily: 'Times New Roman',
                          ),
                        ),
                        Text(
                          "${"2500".toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}.')} FCFA",
                          style: const TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.w300,
                            //fontFamily: 'Times New Roman',
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Frais de livraison :",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.w300,
                            //fontFamily: 'Times New Roman',
                          ),
                        ),
                        Text(
                          "${"500".toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}.')} FCFA",
                          style: const TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.w300,
                            //fontFamily: 'Times New Roman',
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Total :",
                          style: TextStyle(
                            fontSize: 24,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            //fontFamily: 'Times New Roman',
                          ),
                        ),
                        Text(
                          "${"3000".toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}.')} FCFA",
                          style: const TextStyle(
                            fontSize: 24,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            //fontFamily: 'Times New Roman',
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          height: 50,
          child: MaterialButton(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20))),
            height: 6.6,
            color: const Color(0xff0C2705),
            child: const Text(
              "Payer",
              style: TextStyle(
                  //fontFamily: 'Times New Roman',
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            onPressed: () {},
          ),
        ));
  }
}
