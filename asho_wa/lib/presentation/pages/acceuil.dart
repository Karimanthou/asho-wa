import 'dart:async';

import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';

class AcceuilPage extends StatelessWidget {
  AcceuilPage({super.key});
  int activePage = 0;

  late PageController _pageController;
  late Timer _timer;
  List images = [
    "images/chaussure.jpg",
    "images/pull.jpg",
    "images/chemiseCarrole.jpg"
  ];
  void _startAutoScroll() {
    _timer = Timer.periodic(const Duration(seconds: 3), (Timer timer) {
      if (activePage < 2) {
        activePage++;
      } else {
        activePage = 0;
      }
      _pageController.animateToPage(
        activePage,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    _pageController =
        PageController(viewportFraction: 1, initialPage: activePage);
    _startAutoScroll();
    return Scaffold(
      appBar: appBar(context),
      body: boutiqueBody(context),
    );
  }

  appBar(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      foregroundColor: const Color(0xff27272e),
      elevation: 0.0,
      toolbarHeight: 70,
      bottom: PreferredSize(
          preferredSize: const Size.fromHeight(4.0),
          child: Container(
            color: const Color(0x1e425466),
            height: 2.0,
          )),
      //leading: Text(""),
      title: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, Routes.login);
          },
          icon: const Icon(
            Icons.person_pin,
          )),

      actions: [
        const SizedBox(
          width: 7,
        ),
        IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(Routes.produit);
            },
            icon: const Icon(Icons.search_outlined)),
        IconButton(
            onPressed: () {}, icon: const Icon(Icons.notifications_outlined)),
      ],
    );
  }

  Widget boutiqueBody(BuildContext context) {
    return ListView(
      //  padding: const EdgeInsets.all(15),
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Text(
            "Bienvenue à ASHOWA",
            style: titreBienvenu,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: promotionWidget(context, _pageController, images, activePage),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Catégories",
                style: titreGrand,
              ),
              IconButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.produit);
                  },
                  icon: const Icon(Icons.arrow_forward))
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            //mainAxisSize: MainAxisSize.min,
            children: [
              categorie(context, "images/chemise.jpg"),
              categorie(context, "images/chemiseCarrole.jpg"),
              categorie(context, "images/chemise.jpg"),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Tendances",
                style: titreGrand,
              ),
              IconButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.produit);
                  },
                  icon: const Icon(Icons.arrow_forward))
            ],
          ),
        ),
        Container(
            height: 300,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                tendanceWidget(context, "images/chaussure.jpg"),
                tendanceWidget(context, "images/pull.jpg"),
                tendanceWidget(context, "images/lagoste.jpg"),
              ],
            )),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Nouveauté",
                style: titreGrand,
              ),
              IconButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.produit);
                  },
                  icon: const Icon(Icons.arrow_forward))
            ],
          ),
        ),
        Container(
            height: 300,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                tendanceWidget(context, "images/chemise.jpg"),
                tendanceWidget(context, "images/chemiseCarrole.jpg"),
                tendanceWidget(context, "images/chaussure.jpg"),
              ],
            ))
      ],
    );
  }
}
