import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:flutter/material.dart';

class DetailCommandePage extends StatelessWidget {
  const DetailCommandePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar("Détail Commande"),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            const Text(
              "Commande du 06/05/2023",
              style: TextStyle(
                  ////fontFamily: 'Times New Roman',
                  color: Color(0xff323232),
                  fontSize: 20,
                  fontWeight: FontWeight.w400),
            ),
            const SizedBox(height: 10,),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return component();
                },
                itemCount: 3,
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: const ListTile(
        title: Text(
          "Total",
          style: TextStyle(
              ////fontFamily: 'Times New Roman',
              color: Color(0xff323232),
              fontSize: 16,
              fontWeight: FontWeight.w400),
        ),
        trailing: Text(
          "17.500Fcfa",
          style: TextStyle(
              ////fontFamily: 'Times New Roman',
              color: Color(0xff323232),
              fontSize: 15,
              fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
