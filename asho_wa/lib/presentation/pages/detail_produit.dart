import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';

class DetailProduitPage extends StatefulWidget {
  const DetailProduitPage({super.key});

  @override
  State<DetailProduitPage> createState() => _DetailProduitPageState();
}

class _DetailProduitPageState extends State<DetailProduitPage> {
  int nbrevalue = 1;
  int prix = 2500;
  //fonction de soustraction
  void subQte() async {
    if (nbrevalue > 1) {
      setState(() {
        nbrevalue = nbrevalue - 1;
        prix = prix - 2500;
      });
    }
  }

  //fonction d incrementation
  void addQte() async {
    setState(() {
      nbrevalue = nbrevalue + 1;
      prix = prix +2500;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar("Détail Produit"),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          //padding: const EdgeInsets.only(top: 5),
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.4,
                width: MediaQuery.of(context).size.height,
                decoration: boxDecorationImageDetail("images/chaussure.jpg"),
                child: const Text(""),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    const Expanded(
                      child: Text(
                        "Titre produit",
                        style: TextStyle(
                            fontSize: 24,
                            //fontFamily: 'Times New Roman',
                            fontWeight: FontWeight.w500,
                            color: Colors.black),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(
                        Icons.favorite_sharp,
                        size: 22,
                        color: Colors.pink,
                      ),
                      onPressed: () async {},
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 2,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8),
                child: Row(
                  children: const [
                    Expanded(
                      child: Text(
                        "Marque",
                        style: TextStyle(
                            fontSize: 20,
                            //fontFamily: "Times New Roman",
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Text(
                      "Mesure",
                      style: TextStyle(
                          fontSize: 20,
                          //fontFamily: "Times New Roman",
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 2,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8),
                child: Row(
                  children: const [
                    Expanded(
                      child: Text(
                        "Marque",
                        style: TextStyle(
                            fontSize: 20,
                            //fontFamily: "Times New Roman",
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(
                      "Couleur",
                      style: TextStyle(
                          fontSize: 20,
                          //fontFamily: "Times New Roman",
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 2,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8),
                child: Row(
                  children: const [
                    Text(
                      "Nombre :",
                      style: TextStyle(
                          fontSize: 20,
                          //fontFamily: "Times New Roman",
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Container(
                      //  margin: const EdgeInsets.only(left: 3, right: 5),
                      height: 30,
                      width: 110,
                      decoration: const BoxDecoration(
                        color: Color(0xffEEEEEE),
                        borderRadius: BorderRadius.all(
                          Radius.circular(15),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 30,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xffD9D9D9),
                            ),
                            child: IconButton(
                              padding: const EdgeInsets.all(0),
                              onPressed: subQte,
                              icon: const Icon(
                                Icons.remove,
                                color: Color(0xff1A506D),
                                size: 14,
                              ),
                            ),
                          ),
                          Text(
                            nbrevalue.toString(),
                            style: const TextStyle(
                              //fontFamily: 'Times New Roman',
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff1A506D),
                            ),
                          ),
                          Container(
                            width: 30,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xff1A506D),
                            ),
                            child: IconButton(
                              padding: const EdgeInsets.all(0),
                              onPressed: addQte,
                              icon: const Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Text(prix.toString(),
                            style: const TextStyle(
                                fontSize: 32,
                                fontWeight: FontWeight.w600,
                                //fontFamily: 'Times New Roman',
                                color: Colors.black)),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              const Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    "Description :",
                    style: TextStyle(
                        fontSize: 20,
                        //fontFamily: "Times New Roman",
                        fontWeight: FontWeight.w400),
                    // textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: 2,
              ),
              Container(
                padding: const EdgeInsets.only(left: 3, right: 3),
                child: const Text(
                  'description',
                  style: TextStyle(
                    fontSize: 16,
                    //fontFamily: "Times New Roman",
                    fontWeight: FontWeight.w300,
                  ),
                  // textAlign: TextAlign.justify,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar:
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            OutlinedButton(
              style: OutlinedButton.styleFrom(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  foregroundColor: const Color(0xff1A506D),
                  fixedSize: Size(MediaQuery.of(context).size.width * 0.47, 50),
                  side: const BorderSide(color: Color(0xff1A506D)),
                  primary: const Color(0xff1A506D)),
              child:  Text(
                "Personnaliser",
                style: titreMoyen,
              ),
              onPressed: () {
                
              },
            ),
            OutlinedButton(
              style: OutlinedButton.styleFrom(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  backgroundColor: const Color(0xff1A506D),
                  fixedSize: Size(MediaQuery.of(context).size.width * 0.47, 50),
                  side: const BorderSide(color: Color(0xff1A506D)),
                  primary:  Colors.white),
              child:  Text(
                "Ajouter au panier",
                style: titreMoyen,
              ),
              onPressed: () {
               
              },
            ),
          ],
              ),
        ),
    );
  }
}
