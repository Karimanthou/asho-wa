import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';

class PanierPage extends StatelessWidget {
  const PanierPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar("Mon panier"),
        body: Column(
            // scrollDirection: Axis.vertical,
            children: [
              Expanded(
                  child: ListView.builder(
                      itemCount: 2,
                      padding: const EdgeInsets.all(12),
                      itemBuilder: (BuildContext context, int index) {
                        return panierBody(context, "images/bijou.jpg", 2, 5000);
                      })),
              Padding(
                padding: const EdgeInsets.all( 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Nombre de produit : ",
                      style: description,
                    ),
                    Text(
                      "2 ",
                      style: titreMoyen,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Montant Total : ",
                      style: description,
                    ),
                    Text(
                      "${"5000".replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}.')} FCFA ",
                      style: titreMoyen,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 2,
              ),
              MaterialButton(
                minWidth: 100,
                height: 40,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                color: const Color(0xff1A506D),
                child: const Text(
                  "Continuer",
                  style: TextStyle(
                      fontSize: 20,
                      // fontFamily: "Times New Roman",
                      fontWeight: FontWeight.w300,
                      color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(Routes.modeLivraison);
                },
              ),
            ]));
  }
}
