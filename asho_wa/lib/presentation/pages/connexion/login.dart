import 'package:ASHOWA/logic/cubit/inscription/inscription_cubit.dart';
import 'package:ASHOWA/logic/cubit/inscription/inscription_state.dart';
import 'package:ASHOWA/logic/cubit/login/login_cubit.dart';
import 'package:ASHOWA/logic/utils/utils.dart';
import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/body.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);

  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passFocus = FocusNode();
  final GlobalKey<FormState> _loginFormKey = GlobalKey();
  bool? _isSecrete;
  bool _hasError = false;
  bool _isLoading = false;
  String? message;

  @override
  Widget build(BuildContext context) {
    var validate = BlocProvider.of<InscriptionCubit>(context);

    return Scaffold(
        backgroundColor: Colors.white,
        body: BlocConsumer<LoginCubit, LoginState>(
          listener: (context, state) {
            _hasError = state.hasError;
            message = state.messageError;
            Utils.displayData("hasEroorLis", state.hasError);

            if (_hasError) {
              snackBarMessage(context, message!, 10, Colors.redAccent);
            }
          },
          builder: (context, state) {
            _hasError = state.hasError;
            Utils.displayData("hasEroorBuild", state.hasError);

            return Container(
              child: Form(
                key: _loginFormKey,
                child: ListView(
                  padding: const EdgeInsets.fromLTRB(20, 80, 20, 20),
                  children: <Widget>[
                     Text(
                      "Connectez-vous pour continuer",
                      textAlign: TextAlign.left,
                      style: titreBienvenu,
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Container(
                      height: 65,
                      width: 311,
                      child: TextFormField(
                        maxLength: 30,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        //textCapitalization: TextCapitalization.words,
                        controller: _emailController,
                        validator: (value) =>
                            validate.validateEmail(mail: _emailController.text),
                        focusNode: _emailFocus,
                        onFieldSubmitted: (term) {
                          fieldFocusChange(context, _emailFocus, _passFocus);
                        },
                        decoration: InputDecoration(
                            counterText: "",
                            border: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Color(0x99425466))),
                            errorBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Colors.redAccent)),
                            focusedErrorBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide()),
                            enabledBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Color(0x99425466))),
                            focusedBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Color(0x99425466))),
                            // filled: true,

                            fillColor: Colors.grey[100],
                            hintText: "Adresse email"),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    BlocBuilder<InscriptionCubit, InscriptionState>(
                      builder: (context, state) {
                        _isSecrete = state.passVisible;

                        return Container(
                          child: TextFormField(
                            obscureText: !_isSecrete!,
                            textInputAction: TextInputAction.done,
                            focusNode: _passFocus,
                            onFieldSubmitted: (term) {
                  
                            },
                            validator: (value) => validate.validatePassword(
                                password: _passController.text),
                            decoration: InputDecoration(
                                suffixIcon: InkWell(
                                  onTap: () =>
                                      validate.passVisibility(_isSecrete!),
                                  child: Icon(_isSecrete!
                                      ? Icons.visibility_outlined
                                      : Icons.visibility_off_outlined),
                                ),
                                enabledBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Color(0x99425466))),
                                errorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Colors.redAccent)),
                                focusedBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Color(0x99425466))),
                                // filled: true,
                                focusedErrorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Colors.redAccent)),
                                // filled: true,

                                fillColor: Colors.grey[100],
                                hintText: "Mot de passe"),
                            controller: _passController,
                          ),
                        );
                      },
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                            Routes.resetMdp,
                          );
                        },
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child:  Text(
                            "Mot de passe oublié ?",
                            style: titreCategorie,
                          ),
                        )),
                    _isLoading
                        ? Center(
                            child: Column(
                            children: const [
                              CircularProgressIndicator(
                                color: Colors.redAccent,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ],
                          ))
                        : const SizedBox(
                            height: 20,
                          ),
                    BlocBuilder<LoginCubit, LoginState>(
                      builder: (context, state) {
                        return OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            fixedSize: const Size(311, 52),
                            backgroundColor: const Color(0xFF1A506D),
                            primary: const Color(0xfffafafa),
                            textStyle: const TextStyle(
                                fontSize: 14,
                                fontFamily: "SFProDisplay-Semibold"),
                          ),
                          child: Container(
                              child:  Text(
                            "Se connecter",
                            style: titreMoyen,
                          )),
                          onPressed: () {
                            _isLoading = true;
                            if (_loginFormKey.currentState!.validate()) {
                             
                            }
                          },
                        );
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                     // padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                           Center(
                            child: Text(
                              "Vous n’avez pas un compte ?",
                              style: description,
                            ),
                          ),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                  Routes.inscription,
                                );
                              },
                              child:  Text(
                                "Inscrivez-vous",
                                style: titreCategorie,
                              ))
                        ],
                      ),
                    ),
                    
                  ],
                ),
              ),
            );
          },
        ));
  }

  final snackBar = const SnackBar(
    backgroundColor: Color(0xFF1A506D),
    behavior: SnackBarBehavior.floating,
    margin: EdgeInsets.only(left: 20, right: 20),
    dismissDirection: DismissDirection.up,
    content: Text("Vous êtes connecté(e)!"),
    // duration: Duration(milliseconds: 500)
  );

  fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

}
