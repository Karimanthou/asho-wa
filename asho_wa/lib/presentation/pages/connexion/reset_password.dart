import 'package:ASHOWA/logic/cubit/inscription/inscription_cubit.dart';
import 'package:ASHOWA/logic/cubit/inscription/inscription_state.dart';
import 'package:ASHOWA/presentation/widget/appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ResetPassword extends StatelessWidget {
  ResetPassword({super.key});
  final GlobalKey<FormState> _formKeyInformation = GlobalKey();
  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passController = TextEditingController();
  final TextEditingController _passwordConfirmController =
      TextEditingController();

  final FocusNode _passFocus = FocusNode();
  final FocusNode _passwordConfirmFocus = FocusNode();
    final FocusNode _emailFocus = FocusNode();

  bool isSecret = false;
  bool isSecrete = false;
  @override
  Widget build(BuildContext context) {
    var validate = BlocProvider.of<InscriptionCubit>(context);

    return Scaffold(
        appBar: appBar("Réinitilisation du mot de passe"),
        body: BlocBuilder<InscriptionCubit, InscriptionState>(
            builder: (context, state) {
          isSecrete = state.confirmPassVisible;
          isSecret = state.passVisible;
          return Form(
              key: _formKeyInformation,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                   Center(
                     child: Container(
                      height: 65,
                      width: 311,
                      child: TextFormField(
                        maxLength: 30,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        // textCapitalization: TextCapitalization.,
                        controller: _emailController,
                   
                        validator: (value) =>
                            validate.validateEmail(mail: _emailController.text),
                        focusNode: _emailFocus,
                        onFieldSubmitted: (term) {
                          fieldFocusChange(context, _emailFocus, _passFocus);
                        },
                        decoration: InputDecoration(
                            counterText: "",
                            border: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(color: Color(0x99425466))),
                            errorBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(color: Colors.redAccent)),
                            focusedErrorBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide()),
                            enabledBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(color: Color(0x99425466))),
                            focusedBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(color: Color(0x99425466))),
                            // filled: true,
                   
                            fillColor: Colors.grey[100],
                            hintText: "Email"),
                      ),
                                     ),
                   ),
                  const SizedBox(
                    height: 3,
                  ),
                  Center(
                    child: Container(
                        height: 65,
                        width: 311,
                        child: TextFormField(
                          textInputAction: TextInputAction.next,
                          obscureText: !isSecret,
                          decoration: InputDecoration(
                              suffixIcon: InkWell(
                                onTap: () {
                                  validate.passVisibility(isSecret);
                                },
                                child: Icon(isSecret
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined),
                              ),
                              border: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              errorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Colors.redAccent)),
                              focusedErrorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide()),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              // filled: true,
                              fillColor: Colors.grey[100],
                              hintText: "Nouveau mot de passe"),
                          controller: _passController,
                          validator: (value) => validate.validatePassword(
                              password: _passController.text),
                          focusNode: _passFocus,
                          onFieldSubmitted: (term) {
                            fieldFocusChange(
                                context, _passFocus, _passwordConfirmFocus);
                          },
                        )),
                  ),
                  const SizedBox(
                    height: 3,
                  ),
                  Center(
                    child: Container(
                      height: 65,
                      width: 311,
                      child: TextFormField(
                        obscureText: !isSecrete,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            suffixIcon: InkWell(
                              onTap: () {
                                validate.confirmPassvisibility(isSecrete);
                              },
                              child: Icon(isSecrete
                                  ? Icons.visibility_outlined
                                  : Icons.visibility_off_outlined),
                            ),
                            border: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Color(0x99425466))),
                            errorBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Colors.redAccent)),
                            focusedErrorBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide()),
                            enabledBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Color(0x99425466))),
                            focusedBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                borderSide:
                                    BorderSide(color: Color(0x99425466))),
                            // filled: true,
                            fillColor: Colors.grey[100],
                            hintText: "Confirmer le nouveau mot de passe"),
                        controller: _passwordConfirmController,
                        validator: (value) =>
                            validate.validateConfirmationPassword(
                                confirmPass: _passwordConfirmController.text,
                                password: _passController.text),
                        focusNode: _passwordConfirmFocus,
                        onFieldSubmitted: (value) {
                          _passwordConfirmFocus.unfocus();
                          if (_formKeyInformation.currentState!.validate()) {}
                        },
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(30),

                    // margin: EdgeInsets.fromLTRB(18, 0, 20, 0),
                    child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        fixedSize: const Size(311, 52),
                        backgroundColor: const Color(0xFF1A506D),
                        primary: Colors.white,
                        textStyle: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      child: Container(
                          child: const Text(
                        "Enregistrer",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'SFProDisplay-Semibold',
                            color: Color(0xfffafafa)),
                      )),
                      onPressed: () {
                        if (_formKeyInformation.currentState!.validate()) {}
                      },
                    ),
                  ),
                ],
              ));
        }));
  }

  fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
