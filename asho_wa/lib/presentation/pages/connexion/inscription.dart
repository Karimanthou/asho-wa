import 'package:ASHOWA/logic/cubit/inscription/inscription_cubit.dart';
import 'package:ASHOWA/logic/cubit/inscription/inscription_state.dart';
import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage({Key? key}) : super(key: key);

  final GlobalKey<FormState> _formKeyInformation = GlobalKey();

  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _passController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordConfirmController =
      TextEditingController();

  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _passFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _passwordConfirmFocus = FocusNode();
  final TextEditingController _phoneController = TextEditingController();
  var taille;
  var dial = "+229";
  String phone = "";
  bool isSecret = false;
  bool isSecrete = false;
  var message;
  var _hasError;
  bool isload = false;

  @override
  Widget build(BuildContext context) {
    var validate = BlocProvider.of<InscriptionCubit>(context);

    return BlocBuilder<InscriptionCubit, InscriptionState>(
      builder: (context, state) {
        isSecrete = state.confirmPassVisible;
        isSecret = state.passVisible;

        return Scaffold(
          backgroundColor: Colors.white,
          body: Container(
              child: Form(
                  key: _formKeyInformation,
                  child: ListView(
                    padding: const EdgeInsets.fromLTRB(30, 80, 30, 10),
                    children: [
                      Container(
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child:  Text(
                                  "Inscrivez-vous pour\ncommencer",
                                  maxLines: 2,
                                  textAlign: TextAlign.left,
                                  style: titreBienvenu,
                                ),
                              ),
                            ]),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 15),
                        child: Text(
                            "Complétez les informations de votre profil",
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'Inter-Medium',
                                color:
                                    const Color(0xff425466).withOpacity(0.6))),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Container(
                        child: IntlPhoneField(
                          focusNode: _phoneFocus,
                          textInputAction: TextInputAction.next,
                          showDropdownIcon: false,
                          disableLengthCheck: true,
                          initialCountryCode: 'BJ',
                          flagsButtonPadding: const EdgeInsets.only(left: 10),
                          searchText: 'Pays de recherche',
                          invalidNumberMessage:
                              "Veuillez saisir un numéro valide",
                          validator: (value) => validate.validatePhone(
                            phone: _phoneController.text,
                          ),
                          //textInputAction: TextInputAction.next,
                          dropdownTextStyle: const TextStyle(
                              fontFamily: 'SFProDisplay-Regular',
                              color: Color(0xff425466),
                              fontSize: 14),
                          style: const TextStyle(
                              fontFamily: 'SFProDisplay-Regular',
                              color: Color(0xff425466),
                              fontSize: 14),
                          controller: _phoneController,
                          decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              labelStyle: const TextStyle(
                                  fontFamily: 'SFProDisplay-Regular',
                                  color: Color(0xff425466),
                                  fontSize: 14),
                              labelText: 'Téléphone',
                              border: const OutlineInputBorder(
                                borderSide: BorderSide(),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              fillColor: Colors.grey[100],
                              //hintText: "Téléphone",
                              hintStyle: const TextStyle(
                                  fontFamily: 'SFProDisplay-Regular',
                                  color: Color(0xff425466),
                                  fontSize: 14)),
                          onChanged: (phone) {
                            print(phone.completeNumber);
                          },

                          onCountryChanged: (country) {
                            print('Country changed to: ' + country.name);
                            dial = '+' + country.dialCode;
                            print("dial $dial");
                            BlocProvider.of<InscriptionCubit>(context)
                                .getCountryCodeAndMaxNumber(
                                    dialCode: dial,
                                    maxLength: country.maxLength);
                          },
                          onSubmitted: (term) {
                            fieldFocusChange(
                                context, _phoneFocus, _lastNameFocus);
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 9,
                      ),
                      Container(
                        height: 65,
                        width: 311,
                        child: TextFormField(
                          maxLength: 30,
                          //expands: true,
                          textInputAction: TextInputAction.next,
                          validator: (value) => validate.validateFirstName(
                              firstName: _lastNameController.text),
                          focusNode: _lastNameFocus,
                          onFieldSubmitted: (term) {
                            fieldFocusChange(
                                context, _lastNameFocus, _firstNameFocus);
                          },
                          controller: _lastNameController,
                          textCapitalization: TextCapitalization.characters,
                          decoration: InputDecoration(
                              //prefix: Icon(Icons.),
                              counter: const SizedBox(
                                height: 0.0,
                              ),
                              errorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Colors.redAccent)),
                              focusedErrorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide()),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              // filled: true,

                              fillColor: Colors.grey[100],
                              hintText: "Nom"),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Container(
                        height: 65,
                        width: 311,
                        child: TextFormField(
                          maxLength: 30,
                          keyboardType: TextInputType.name,
                          textInputAction: TextInputAction.next,
                          textCapitalization: TextCapitalization.words,
                          controller: _firstNameController,
                          validator: (value) => validate.validateLastName(
                              lastName: _firstNameController.text),
                          focusNode: _firstNameFocus,
                          onFieldSubmitted: (term) {
                            fieldFocusChange(
                                context, _firstNameFocus, _emailFocus);
                          },
                          decoration: InputDecoration(
                              counterText: "",
                              border: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              errorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Colors.redAccent)),
                              focusedErrorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide()),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              // filled: true,

                              fillColor: Colors.grey[100],
                              hintText: "Prénom"),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Container(
                        height: 65,
                        width: 311,
                        child: TextFormField(
                          maxLength: 30,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.next,
                          // textCapitalization: TextCapitalization.,
                          controller: _emailController,

                          validator: (value) => validate.validateEmail(
                              mail: _emailController.text),
                          focusNode: _emailFocus,
                          onFieldSubmitted: (term) {
                            fieldFocusChange(
                                context, _emailFocus, _passFocus);
                          },
                          decoration: InputDecoration(
                              counterText: "",
                              border: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              errorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Colors.redAccent)),
                              focusedErrorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide()),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              // filled: true,

                              fillColor: Colors.grey[100],
                              hintText: "Email"),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                   
                      Container(
                          height: 65,
                          width: 311,
                          child: TextFormField(
                            textInputAction: TextInputAction.next,
                            obscureText: !isSecret,
                            decoration: InputDecoration(
                                suffixIcon: InkWell(
                                  onTap: () {
                                    validate.passVisibility(isSecret);
                                  },
                                  child: Icon(isSecret
                                      ? Icons.visibility_outlined
                                      : Icons.visibility_off_outlined),
                                ),
                                border: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Color(0x99425466))),
                                errorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Colors.redAccent)),
                                focusedErrorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide()),
                                enabledBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Color(0x99425466))),
                                focusedBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide:
                                        BorderSide(color: Color(0x99425466))),
                                // filled: true,
                                fillColor: Colors.grey[100],
                                hintText: "Mot de passe"),
                            controller: _passController,
                            validator: (value) => validate.validatePassword(
                                password: _passController.text),
                            focusNode: _passFocus,
                            onFieldSubmitted: (term) {
                              fieldFocusChange(
                                  context, _passFocus, _passwordConfirmFocus);
                            },
                          )),
                      const SizedBox(
                        height: 3,
                      ),
                      Container(
                        height: 65,
                        width: 311,
                        child: TextFormField(
                          obscureText: !isSecrete,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                              suffixIcon: InkWell(
                                onTap: () {
                                  validate.confirmPassvisibility(isSecrete);
                                },
                                child: Icon(isSecrete
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined),
                              ),
                              border: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              errorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Colors.redAccent)),
                              focusedErrorBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide()),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(color: Color(0x99425466))),
                              // filled: true,
                              fillColor: Colors.grey[100],
                              hintText: "Confirmer le mot de passe"),
                          controller: _passwordConfirmController,
                          validator: (value) =>
                              validate.validateConfirmationPassword(
                                  confirmPass: _passwordConfirmController.text,
                                  password: _passController.text),
                          focusNode: _passwordConfirmFocus,
                          onFieldSubmitted: (value) {
                            _passwordConfirmFocus.unfocus();
                            if (_formKeyInformation.currentState!.validate() ) {
                          
                            }
                          },
                        ),
                      ),
                    ],
                  ))),
          bottomNavigationBar: Container(
            padding: const EdgeInsets.all(30),

            // margin: EdgeInsets.fromLTRB(18, 0, 20, 0),
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4))),
                fixedSize: const Size(311, 52),
                backgroundColor: const Color(0xFF1A506D),
                primary: Colors.white,
                textStyle: const TextStyle(
                  fontSize: 14,
                ),
              ),
              child: Container(
                  child: const Text(
                "Enregistrer",
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: 'SFProDisplay-Semibold',
                    color: Color(0xfffafafa)),
              )),
              onPressed: () {
               

                if (_formKeyInformation.currentState!.validate()) {
                 
                }
              },
            ),
          ),
        );
      },
    );
  }

  fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  final snackBarSuccess = const SnackBar(
    behavior: SnackBarBehavior.floating,
    margin: EdgeInsets.only(left: 30, right: 30),
    backgroundColor: Color(0xff642c8f),
    dismissDirection: DismissDirection.up,
    content: Text("Inscription réussie"),
    //duration: Duration(milliseconds: 200)
  );

  sucessDialog(BuildContext context) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SizedBox(
          height: 200,
          width: 300,
          child: SimpleDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            alignment: Alignment.center,
            title: Container(
              child: Column(
                children: [
                  const Text(
                    "Inscription réussie",
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: 'SFProDisplay-Bold',
                        color: Color(0xff27272e)),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    /* padding: EdgeInsets.only(
                                                      left: 12, right: 12), */
                    child: RichText(
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 5,
                      text: const TextSpan(
                        text: 'Merci de vous être inscrit à Marketo.',
                        style: TextStyle(
                            fontSize: 13,
                            fontFamily: 'SFProDisplay-Medium',
                            color: Color(0xff8c96a1)),
                        children: [
                          TextSpan(
                            text: ' Veuillez vous connecter ',
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'SFProDisplay-Medium',
                                color: Color(0xff8c96a1)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {},
                child: Container(
                  padding: const EdgeInsets.all(0),
                  child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      shape: const StadiumBorder(),
                      //fixedSize: const Size(139, 30),
                      primary: const Color(0xFF1A506D),
                      side: const BorderSide(color: Color(0xFF1A506D)),
                      textStyle: const TextStyle(
                          fontSize: 14, fontFamily: 'SFProDisplay-Semibold'),
                    ),
                    child: const Text("Se connecter"),
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                        Routes.login,

                        (Route<dynamic> route) => false,
                        // arguments: sucessDialog(context),
                        //arguments: {"key": "value"}
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
