import 'package:ASHOWA/logic/cubit/home/home_cubit.dart';
import 'package:ASHOWA/presentation/pages/commande.dart';
import 'package:ASHOWA/presentation/pages/connexion/change_password.dart';
import 'package:ASHOWA/presentation/pages/connexion/inscription.dart';
import 'package:ASHOWA/presentation/pages/connexion/login.dart';
import 'package:ASHOWA/presentation/pages/connexion/reset_password.dart';
import 'package:ASHOWA/presentation/pages/detail_commande.dart';
import 'package:ASHOWA/presentation/pages/detail_produit.dart';
import 'package:ASHOWA/presentation/pages/home_page.dart';
import 'package:ASHOWA/presentation/pages/produit.dart';
import 'package:ASHOWA/presentation/pages/profil.dart';
import 'package:ASHOWA/presentation/pages/reception.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (_) {
            BlocProvider.of<HomeCubit>(_).onItemTapped(0);

            return const MyHomePage(tabIndex: 0);
          },
        );
      case '/panier':
        return MaterialPageRoute(
          builder: (_) {
            BlocProvider.of<HomeCubit>(_).onItemTapped(1);

            return const MyHomePage(tabIndex: 1);
          },
        );
      case '/favori':
        return MaterialPageRoute(
          builder: (_) {
            BlocProvider.of<HomeCubit>(_).onItemTapped(2);

            return const MyHomePage(tabIndex: 2);
          },
        );
      case '/parametre':
        return MaterialPageRoute(
          builder: (_) {
            BlocProvider.of<HomeCubit>(_).onItemTapped(3);

            return const MyHomePage(tabIndex: 3);
          },
        );
      case '/produit':
        return MaterialPageRoute(
          builder: (_) {
            return ProduitPage();
          },
        );
      case '/detailProduit':
        return MaterialPageRoute(
          builder: (_) {
            return DetailProduitPage();
          },
        );
      case '/modeLivraison':
        return MaterialPageRoute(
          builder: (_) {
            return Reception();
          },
        );
      case '/mesCommandes':
        return MaterialPageRoute(
          builder: (_) {
            return MesCommandesPage();
          },
        );
      case '/detailCommande':
        return MaterialPageRoute(
          builder: (_) {
            return DetailCommandePage();
          },
        );
      case '/profil':
        return MaterialPageRoute(
          builder: (_) {
            return Profil();
          },
        );
      case '/changementMdp':
        return MaterialPageRoute(
          builder: (_) {
            return ChangementPassword();
          },
        );
      case '/resetMdp':
        return MaterialPageRoute(
          builder: (_) {
            return ResetPassword();
          },
        );
      case '/login':
        return MaterialPageRoute(
          builder: (_) {
            return LoginPage();
          },
        );
      case '/inscription':
        return MaterialPageRoute(
          builder: (_) {
            return RegisterPage();
          },
        );

      default:
        return null;
    }
  }
}
