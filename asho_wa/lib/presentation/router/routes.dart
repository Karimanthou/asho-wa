class Routes {
  static const String accueil = '/';
  static const String panier = '/panier';
  static const String favori = '/favori';
  static const String parametre = '/parametre';
  static const String produit = '/produit';
  static const String detailProduit = '/detailProduit';
  static const String modeLivraison = '/modeLivraison';
  static const String mesCommandes = '/mesCommandes';
  static const String detailCommande = '/detailCommande';
  static const String inscription = '/inscription';
  static const String profil = '/profil';
  static const String changementMdp = '/changementMdp';
  static const String resetMdp = '/resetMdp';
  static const String login = '/login';

}
