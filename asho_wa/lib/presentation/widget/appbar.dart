import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';

appBar(String titre) {
  return AppBar(
    backgroundColor: Colors.white,
    foregroundColor: const Color(0xff27272e),
    elevation: 0.0,
    toolbarHeight: 70,
    bottom: PreferredSize(
        preferredSize: const Size.fromHeight(4.0),
        child: Container(
          color: const Color(0x1e425466),
          height: 2.0,
        )),
    //leading: Text(""),
    title:Center(child: Text(titre,style: titreGrand,),)
  );
}
