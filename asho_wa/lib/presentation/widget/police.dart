import 'package:flutter/material.dart';

TextStyle description = const TextStyle(
    fontSize: 16, fontFamily: 'SFProDisplay-medium', color: Color(0xff5D5C69));
TextStyle titreMoyen = const TextStyle(
  fontSize: 16,
  fontFamily: 'SFProDisplay-Semibold',
);
TextStyle titreCategorie = const TextStyle(
    fontSize: 16,
    fontFamily: 'SFProDisplay-Semibold',
    color: Color(0xff1A506D));
TextStyle titreGrand = const TextStyle(
    fontSize: 20, fontFamily: 'SFProDisplay-Bold', color: Color(0xff1A506D));
TextStyle titreBienvenu = const TextStyle(
    fontSize: 30, fontFamily: 'SFProDisplay-Bold', color: Color(0xff1A506D));

BoxDecoration boxDecorationImage(String image) {
  return BoxDecoration(
      image: DecorationImage(fit: BoxFit.cover, image: AssetImage(image)),
      borderRadius: const BorderRadius.all(
        Radius.circular(10),
      ),
      shape: BoxShape.rectangle);
}
BoxDecoration boxDecorationImageDetail(String image) {
  return BoxDecoration(
      image: DecorationImage(fit: BoxFit.cover, image: AssetImage(image)),
      borderRadius: const BorderRadius.only(
      bottomLeft:  Radius.circular(15),
       bottomRight: Radius.circular(15),
      ),
      shape: BoxShape.rectangle);
}

BoxDecoration boxDecorationSimple() {
  return BoxDecoration(
      color: const Color(0xff000000).withOpacity(0.1),
      border: Border.all(
        color: const Color(0xff000000).withOpacity(0.0),
      ),
      borderRadius: const BorderRadius.all(
        Radius.circular(10),
      ),
      shape: BoxShape.rectangle);
}
BoxDecoration boxDecorationPromotion(Color color) {
  return BoxDecoration(
      color: color.withOpacity(0.3),
      border: Border.all(
        color: const Color(0xff000000).withOpacity(0.0),
      ),
      borderRadius: const BorderRadius.all(
        Radius.circular(10),
      ),
      shape: BoxShape.rectangle);
}
