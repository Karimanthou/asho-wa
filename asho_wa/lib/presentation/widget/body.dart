import 'package:ASHOWA/presentation/router/routes.dart';
import 'package:ASHOWA/presentation/widget/police.dart';
import 'package:flutter/material.dart';

void subQte(int nbrevalue, int prix) async {
  if (nbrevalue > 1) {
    //setState(() {
    nbrevalue = nbrevalue - 1;
    prix = prix - 2500;
    // });
  }
}

//fonction d incrementation
void addQte(int nbrevalue, int prix) async {
  //setState(() {
  nbrevalue = nbrevalue + 1;
  prix = prix + 2500;
  // });
}

Widget categorie(BuildContext context, String image) {
  return TextButton(
    onPressed: () {
      Navigator.of(context).pushNamed(Routes.produit);
    },
    child: Container(
      padding: const EdgeInsets.only(top: 10),
      height: 130,
      width: MediaQuery.of(context).size.width * 0.29,
      decoration: boxDecorationSimple(),
      child: Column(
        children: [
          Text(
            "Catégorie 1",
            style: titreCategorie,
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: Container(
              // height: 80,
              width: MediaQuery.of(context).size.width * 0.29,
              decoration: boxDecorationImage(image),

              child: const Text(''),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget tendanceWidget(BuildContext context, String image) {
  return TextButton(
    onPressed: () {
      Navigator.of(context).pushNamed(Routes.detailProduit);
    },
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 2),
          width: MediaQuery.of(context).size.width * 0.58,
          height: 200,
          decoration: boxDecorationImage(image),
          child: const Text(""),
        ),
        Container(
          margin: const EdgeInsets.only(left: 2),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 5,
              ),
              Text(
                "Titre du produit",
                style: titreMoyen,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "Description",
                maxLines: 1,
                style: description,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "12 000 XOF",
                style: titreMoyen,
              ),
            ],
          ),
        )
      ],
    ),
  );
}

Widget promotionWidget(BuildContext context, PageController pageController,
    List lists, int activePage) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 200,
    decoration: boxDecorationPromotion(const Color(0xff1A506D)),
    child: PageView.builder(
        itemCount: lists.length,
        pageSnapping: true,
        controller: pageController,
        onPageChanged: (page) {
          //setState(() {
          activePage = page;
          //});
        },
        itemBuilder: (context, pagePosition) {
          return Column(
            children: [
              Container(
                height: 180,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding:
                          const EdgeInsets.only(top: 10, left: 10, right: 10),
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Le meilleur de vêtement pour votre look",
                            style: TextStyle(
                                color: Color(0xff1A506D),
                                fontSize: 22,
                                fontWeight: FontWeight.w500),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "-25%",
                            style: titreGrand,
                          ),
                          Text(
                            "à l'achat",
                            style: titreCategorie,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: Center(
                            child:
                                Image(image: AssetImage(lists[pagePosition]))))
                  ],
                ),
              ),
              /* Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: indicators(lists.length, activePage)), */
            ],
          );
        }),
  );
}

List<Widget> indicators(imagesLength, currentIndex) {
  return List<Widget>.generate(imagesLength, (index) {
    return Container(
      margin: const EdgeInsets.all(2),
      width: currentIndex == index ? 20 : 12,
      height: currentIndex == index ? 10 : 8,
      decoration: BoxDecoration(
          color: currentIndex == index
              ? const Color(0xffFF8473)
              : const Color(0xffFFC0B8),
          borderRadius: const BorderRadius.all(
            Radius.circular(16),
          ),
          shape: BoxShape.rectangle),
    );
  });
}

Widget bodyFavori(BuildContext context, String image) {
  return Column(
    children: [
      ListTile(
        onTap: () {
          Navigator.of(context).pushNamed(Routes.detailProduit);
        },
        contentPadding: const EdgeInsets.symmetric(vertical: 5),
        leading: Container(
          height: 56,
          width: 56,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(10),
            ),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        title: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              "Nom du Produit",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                // fontFamily: 'SFProDisplay-Bold',
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "2.500Fcfa",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                // fontFamily: 'SFProDisplay-Bold',
              ),
            ),
          ],
        ),
        trailing: IconButton(
            onPressed: () async {}, icon: const Icon(Icons.cancel_outlined)),
      ),
      const Divider(
        thickness: 0.5,
        color: Colors.black,
      ),
    ],
  );
}

Widget panierBody(BuildContext context, String image, int nbrevalue, int prix) {
  return Builder(builder: (context) {
    return Column(
      children: [
        Row(
          children: [
            Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(15.0),
                  child: Image.asset(
                    image,
                    height: 80,
                    width: 80,
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
            const SizedBox(
              width: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Nom produit',
                  style: titreMoyen,
                ),
                const SizedBox(
                  height: 25,
                ),
                Text(
                  "${prix.toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}.')} FCFA",
                  style: const TextStyle(
                    fontSize: 15,
                    // fontFamily: 'Times New Roman',
                  ),
                ),
              ],
            ),
            const Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.cancel_outlined,
                    size: 24,
                  ),
                ),
                const SizedBox(
                  height: 25,
                ),
                Container(
                  height: 22.5,
                  width: 81,
                  decoration: const BoxDecoration(
                    color: Color(0xffEEEEEE),
                    borderRadius: BorderRadius.all(
                      Radius.circular(11.5),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 24,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xffD9D9D9),
                        ),
                        child: IconButton(
                          padding: const EdgeInsets.all(0),
                          onPressed: () {
                            subQte(nbrevalue, prix);
                          },
                          icon: const Icon(
                            Icons.remove,
                            color: Color(0xff1A506D),
                            size: 14,
                          ),
                        ),
                      ),
                      Text(nbrevalue.toString(), style: titreCategorie),
                      Container(
                        width: 24,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff1A506D),
                        ),
                        child: IconButton(
                          padding: const EdgeInsets.all(0),
                          onPressed: () {
                            addQte(nbrevalue, prix);
                          },
                          icon: const Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        const Divider(
          thickness: 0.5,
          color: Colors.black,
        ),
      ],
    );
  });
}

Widget parametreBody(String title, IconData icon, Function() onTap) {
  return Column(
    children: [
      ListTile(
        onTap: onTap,
        leading: Icon(icon),
        title: Text(
          title,
          style: titreMoyen,
        ),
      ),
      const Divider(
        height: 2,
        thickness: 0.5,
        //indent: 5,
        // endIndent: 5,
        color: Colors.black,
      ),
    ],
  );
}

Widget commandeBody(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      ListTile(
        onTap: () {
          Navigator.pushNamed(context, Routes.detailCommande);
        },
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "06/05/2023",
              style: description,
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              "17500Fcfa",
              style: titreMoyen,
            ),
          ],
        ),
        trailing: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(
              Icons.fiber_manual_record,
              color: Colors.black,
              size: 10,
            ),
            const SizedBox(
              width: 3,
            ),
            Text(
              "Statut",
              style: description,
            ),
          ],
        ),
      ),
      const Divider(
        height: 5,
        thickness: 0.5,
        //indent: 5,
        // endIndent: 5,
        color: Colors.black,
      ),
    ],
  );
}

Widget component() {
  return Column(
    children: [
      ListTile(
        contentPadding: const EdgeInsets.symmetric(vertical: 5),
        leading: Container(
          height: 56,
          width: 56,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            image: DecorationImage(
              image: AssetImage("images/bijou.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: const Text(""),
        ),
        title: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              "Titre de ce produit",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                // fontFamily: 'SFProDisplay-Bold',
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "18.500Fcfa",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                // fontFamily: 'SFProDisplay-Bold',
              ),
            ),
          ],
        ),
      ),
      const Divider(
        thickness: 0.5,
        color: Colors.black,
      ),
    ],
  );
}
snackBarMessage(
    BuildContext context, String message, double margeBottom, Color colors) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    backgroundColor: colors,
    dismissDirection: DismissDirection.up,
    behavior: SnackBarBehavior.floating,
    margin: EdgeInsets.only(left: 30, right: 30, bottom: margeBottom),
    content: Text(
      message.toString(),

      // textAlign: TextAlign.center,
    ),
    //duration: Duration(milliseconds: 200)
  ));
}
