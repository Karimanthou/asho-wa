import 'package:ASHOWA/logic/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'presentation/router/app_router.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
   MyApp({super.key});
final AppRouter _appRouter = AppRouter();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Map<int, Color> color = {
      50: const Color(0xFF1A506D),
      100: const Color(0xFF1A506D),
      200: const Color(0xFF1A506D),
      300: const Color(0xFF1A506D),
      400: const Color(0xFF1A506D),
      500: const Color(0xFF1A506D),
      600: const Color(0xFF1A506D),
      700: const Color(0xFF1A506D),
      800: const Color(0xFF1A506D),
      900: const Color(0xFF1A506D),
    };
    return MultiBlocProvider(
        providers: blocProviders,
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'ASHOWA',
          theme: ThemeData(
            primarySwatch: MaterialColor(0xFF1A506D, color),
            primaryColor: const Color(0xFF1A506D),
          ),
          onGenerateRoute: _appRouter.onGenerateRoute,
         
        ));
  }
}

